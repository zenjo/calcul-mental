#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fileencoding=utf-8
#
#  ex_additions.py
#
#  Copyright 2019 Robert Sebille <robert@sebille.name>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

import random
random.seed()


class ExArithm1():
    """Cette classe propose des exercices divers d'arithmétique"""

    # Constructeur
    def __init__(self, titre=""):
        self.titre = titre

    @classmethod
    def clear(cls):
        """Efface l'écran et amène le curseur à 0, 0"""
        print("\033[2J" + "\033[0;0f")

    ############
    # Privates #
    ############
    def _verifie_mini_maxi(self, mini, maxi):
        """Vérifie si mini < maxi, sinon les inverse"""
        if mini > maxi:
            maxi, mini = mini, maxi
        return mini, maxi

    def _exercices_a_2_nombres(self, nbfois, mini, maxi, operateur, point):
        """
        Exécute l'exercice, entrée de l'utilisateur protégée

        - recoit le nombre de questions (nbfois), le range
        des opérandes (mini, maxi), l'opérateur (" + ", " - ", " x ", " : ")
        et le nombre de point par question.
        - retourne la note et le dictionnaire des résultats de l"exercice
        Ces retours pourront être encodé pour impression, dans un objet
        Personne: self.traces à l'aide de la méthode:
        sauve_traces(self, traces)
        """

        note = 0
        details = {}
        for c in range(nbfois):
            # init
            i = random.randint(mini, maxi)
            if operateur == " - ":
                j = random.randint(1, mini)
                resultat = i - j
            else:
                j = random.randint(mini, maxi)
            if operateur == " + ":
                resultat = i + j
            if operateur == " x ":
                resultat = i * j
            if operateur == " : ":
                resultat = i * j
                resultat, i = i, resultat

            # on construit une boucle while qui ne plante pas quelle que
            # soit l'entrée'
            rep = ""
            while not rep.isnumeric():
                rep = input(f"{c + 1} Combien font {i} {operateur} {j} ? ")
            rep = int(rep)

            # On construit la note totale de l'exercice et le dictionnaire
            # des résultats particuliers
            details[c+1] = f"{i} {operateur} {j} =  {rep} -> "
            if int(rep) == resultat:
                note += point
                details[c+1] += f"{point} point(s)."
            else:
                details[c+1] += "0 point(s)."
        return note, details

    ###########
    # Publics #
    ###########
    ########################
    # Exercices d'addition #
    def addition(self, mini=1, maxi=10, nbfois=5, point=1):

        # On corrige une éventuelle erreur d'inverion sur mini et maxi
        mini, maxi = self._verifie_mini_maxi(mini, maxi)
        # on construit un titre dynamiqe à l'aide des paramètres et à imprimer
        self.titre_dynamique = (f"Addition de 2 nombres entre {mini} et \
{maxi} ({nbfois} fois)")
        print(f"{self.titre_dynamique}\n {point} point(s) par question : \n")

        # L'exercice proprement dit
        note, details = self._exercices_a_2_nombres(nbfois, mini, maxi,
                                                    " + ", point)

        # On retourne un tuple avec la note totale de l'exercice, le
        # dictionnaire des résutats particuliers le titre dynamique
        # de l'exercice, et le total des points de l'exercice
        points = nbfois * point
        return note, details, self.titre_dynamique, points

    #############################
    # Exercices de soustraction #
    def soustraction(self, mini=10, maxi=20, nbfois=5, point=1):
        # On corrige une éventuelle erreur d'inverion sur mini et maxi
        mini, maxi = self._verifie_mini_maxi(mini, maxi)
        # on construit un titre dynamiqe à l'aide des paramètres et à imprimer
        self.titre_dynamique = (f"Soustraction de 2 nombres entre [{mini}..\
{maxi} ] et [1..{mini}] ({nbfois} fois)")
        print(f"{self.titre_dynamique}\n{point} point(s) par question : \n")

        # L'exercice proprement dit
        note, details = self._exercices_a_2_nombres(nbfois, mini, maxi,
                                                    " - ", point)

        # On retourne un tuple avec la note totale de l'exercice, le
        # dictionnaire des résutats particuliers le titre dynamique
        # de l'exercice, et le total des points de l'exercice
        points = nbfois * point
        return note, details, self.titre_dynamique, points

    ###############################
    # Exercices de multiplication #
    def multiplication(self, mini=10, maxi=20, nbfois=5, point=1):
        # On corrige une éventuelle erreur d'inverion sur mini et maxi
        mini, maxi = self._verifie_mini_maxi(mini, maxi)
        # on construit un titre dynamiqe à l'aide des paramètres et à imprimer
        self.titre_dynamique = (f"Multiplication de 2 nombres compris entre \
{mini} et {maxi} ({nbfois} fois)")
        print(f"{self.titre_dynamique}\n{point} point(s) par question : \n")

        # L'exercice proprement dit
        note, details = self._exercices_a_2_nombres(nbfois, mini, maxi,
                                                    " x ", point)
        # On retourne un tuple avec la note totale de l'exercice, le
        # dictionnaire des résutats particuliers le titre dynamique
        # de l'exercice, et le total des points de l'exercice
        points = nbfois * point
        return note, details, self.titre_dynamique, points

    #################################
    # Exercices de division entière #
    def division(self, mini=10, maxi=20, nbfois=5, point=1):
        # On corrige une éventuelle erreur d'inverion sur mini et maxi
        mini, maxi = self._verifie_mini_maxi(mini, maxi)
        # on construit un titre dynamiqe à l'aide des paramètres et à imprimer
        self.titre_dynamique = (f"Division entière de 2 nombres où <nombre> \
divisé par [{mini}..{maxi}] = [{mini}..{maxi}] ({nbfois} fois)")
        print(f"{self.titre_dynamique}\n{point} point(s) par question : \n")

        # L'exercice proprement dit
        note, details = self._exercices_a_2_nombres(nbfois, mini, maxi,
                                                    " : ", point)
        # On retourne un tuple avec la note totale de l'exercice, le
        # dictionnaire des résutats particuliers le titre dynamique
        # de l'exercice, et le total des points de l'exercice
        points = nbfois * point
        return note, details, self.titre_dynamique, points

    ########################################
    # Exercices de table de multiplication #
    def table(self, par=2, maxi=10, point=1):

        # On corrige une éventuelle erreur d'inverion sur mini et maxi
        par, maxi = self._verifie_mini_maxi(par, maxi)
        # on construit un titre dynamiqe à l'aide des paramètres et à imprimer
        self.titre_dynamique = (f"Table de multiplication par {par}, jusqu'à \
{maxi}")
        print(f"{self.titre_dynamique}\n {point} point(s) par question : \n")

        # L'exercice proprement dit
        note = 0
        details = {}
        for i in range(maxi):
            resultat = (i+1) * par
            rep = ""
            while not rep.isnumeric():
                rep = input(f"{i+1} Combien font {i+1} x {par} ? ")
            rep = int(rep)

            details[i+1] = f"{i+1} x {par} =  {rep} -> "
            if rep == resultat:
                note += point
                details[i+1] += f"{point} point(s)."
            else:
                details[i+1] += "0 point(s)."

        # On retourne un tuple avec la note totale de l'exercice, le
        # dictionnaire des résutats particuliers le titre dynamique
        # de l'exercice, et le total des points de l'exercice
        points = maxi * point
        return note, details, self.titre_dynamique, points

    def format_resultat(self, resultat):
        """
        On formate les résulats pour ces exercices ci
        """

        res = ""
        ###################################################################
        # resultat[0] = note de l'exercice courant                        #
        # resultat[1] = les détails des résultats à l'exercice courant    #
        # resultat[2] = le titre dynamique renvoyé par l'exercice courant #
        # resultat[3] = le total des points de l'exercice courant         #
        ###################################################################

        # Le total de la note de l'exercice actuel
        total_points_courant = resultat[3]
        # la note pour l'exercice courant
        note_courante = resultat[0]
        # La variable de suivi des résultats de la personne
        # (à sauver en fichier?)
        res += f"\n\n{resultat[2]}, résultats :\n\n"
        res += f"Note courante : {note_courante}/{total_points_courant}\n"
        res += "Detail:\n"
        for k, v in resultat[1].items():
            res += f"Ex. {k}: {v}\n"
        return res


if __name__ == '__main__':
    pass
