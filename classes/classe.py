#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fileencoding=utf-8
#
#  classe.py
#
#  Copyright 2019 Robert Sebille <robert@sebille.name>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#


class Classe():
    """Cette classe gère les attributs des classes de l'école
    Elle hérite de la classe Exercice pour en faciliter l'accès aux
    personnes (voir cette classe)
    """

    # Constructeur
    def __init__(self, nom):
        self.nom = nom
        # dictionnaire des élèves de cette classe
        # key = eid, value = objets Personnes
        self._eleves = {}

    # Privates
    def __repr__(self):
        return self.nom

    def __str__(self):
        return self.nom

    # properties
    @property
    def eleves(self):
        return self._eleves

    @eleves.setter
    def eleves(self, personne):
        self._eleves[personne.eid] = personne


if __name__ == '__main__':
    pass
