#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fileencoding=utf-8
#
#  personne.py
#
#  Copyright 2019 Robert Sebille <robert@sebille.name>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

if __name__ == '__main__':
    from classe import Classe
else:
    from classes.classe import Classe
pass


class Personne(Classe):
    """Cette classe gère les attributs des élèves de l'école
    Elle hérite de la classe Classe pour permettre d'atacher un élève
    à une classe. La classe Classe hérite de la classe Exercice pour
    permettre aux élèves d'accéder facilement aux exercices
    """

    # Constructeur
    def __init__(self, eid, nom, classe):
        Classe.__init__(self, classe)
        if eid in classe.eleves:
            print(f"Je ne peux pas créer la personne {nom}, id = {eid},")
            print(f"car cette id existe déjà :")
            print(f"{classe.eleves[eid]}, id = {classe.eleves[eid].eid}.")
            print("Programme stoppé avec le code 1")
            del self
            exit(1)
        self._eid = eid
        self.nom = nom
        self._traces = ""
        self.note_totale = 0
        self.total_points = 0
        self.classe = classe
        classe.eleves = self

    # Private
    def __repr__(self):
        return self.nom

    def __str__(self):
        return self.nom + ', ' + str(self.classe)

    @property
    def eid(self):
        return self._eid

    @property
    def traces(self):
        """
        Résultats de l'instance
        """
        return self._traces

    @traces.setter
    def traces(self, traces):
        """
        On sauve les résulats de l'instance
        Il faudra rendre cela persistant (base de données, fichier, ...)
        """
        self._traces += traces

    # Publics
    def calcul_note_generale(self, resultat):
        self.note_totale += resultat[0]
        self.total_points += resultat[3]
