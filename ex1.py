#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fileencoding=utf-8
#
#  main.py
#
#  Copyright 2019 Robert Sebille <robert@sebille.name>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#


from classes.personne import Personne
from classes.classe import Classe
from classes.ex_arithm1 import ExArithm1


if __name__ == '__main__':
    # 1. Initialisation
    classe = Classe("Classe une")
    rob = Personne("id1", "Robert Sebille", classe)
    jul = Personne("id2", "Jules Romain", classe)
    # Les id doivent être uniques
    # plou = Personne("id2", "Ploum Ploum", classe)

    # Créons une instance de la classe ExArithm1
    e = ExArithm1()

    # Efface l'écran
    ExArithm1.clear()

    # Note explicative:
    print("""
    Petite demo d'utilisation des classes Classe et Personne:

    si j'ai fait:
classe = Classe("Classe une")
rob = Personne("id1", "Robert Sebille", classe)
jul = Personne("id2", "Jules Romain", classe)

    alors,
print(rob.classe)
print(classe.eleves)
print(f"type(classe.eleves['id1']) = {type(classe.eleves['id1'])}")
print(f"{classe.eleves['id2'].nom} est dans la classe \\
{classe.eleves['id2'].classe}")
print(f"{rob.nom}, id {rob.eid} et {classe.eleves['id1']}")

    va retourner:
    """)

    print(rob.classe)
    print(classe.eleves)
    print(f"type(classe.eleves['id1']) = {type(classe.eleves['id1'])}")
    print(f"{classe.eleves['id2'].nom} est dans la classe \
{classe.eleves['id2'].classe}")
    print(f"{rob.nom}, id {rob.eid} et {classe.eleves['id1']}")

    input("\nPressez Entrée pour continuer")
    ExArithm1.clear()

    print("Maintenant, testons :")
    print("--------------------")

    # 2. Exercices
    print(f"Pour {rob.nom} :")
    resultat = e.addition(3, 5, 3)
    rob.calcul_note_generale(resultat)
    rob.traces = e.format_resultat(resultat)
    print(f"\nNote exercice : {resultat[0]}/{resultat[3]}")
    print(f"\nNote totale : {rob.note_totale}/{rob.total_points}")
    input("\nPressez Entrée pour continuer")

    ExArithm1.clear()

    print(f"Encore pour {rob.nom} :")
    resultat = e.soustraction(5, 10, 3, 2)
    rob.calcul_note_generale(resultat)
    rob.traces = e.format_resultat(resultat)
    print(f"\nNote exercice : {resultat[0]}/{resultat[3]}")
    print(f"\nNote totale : {rob.note_totale}/{rob.total_points}")
    input("\nPressez Entrée pour continuer")

    ExArithm1.clear()
    print("--------------------")

    print(f"Et maintenant pour {jul.nom} :")
    resultat = e.multiplication(5, 10, 3, 3)
    jul.calcul_note_generale(resultat)
    jul.traces = e.format_resultat(resultat)
    print(f"\nNote exercice : {resultat[0]}/{resultat[3]}")
    print(f"\nNote totale : {jul.note_totale}/{jul.total_points}")
    input("\nPressez Entrée pour continuer")

    ExArithm1.clear()

    print(f"Encore pour {jul.nom} :")
    resultat = e.division(10, 20, 3, 5)
    jul.calcul_note_generale(resultat)
    jul.traces = e.format_resultat(resultat)
    print(f"\nNote exercice : {resultat[0]}/{resultat[3]}")
    print(f"\nNote totale : {jul.note_totale}/{jul.total_points}")
    input("\nPressez Entrée pour continuer")

    ExArithm1.clear()

    print(f"Pour terminer, toujours pour {jul.nom} :")
    resultat = e.table(9, 10, 3)
    jul.calcul_note_generale(resultat)
    jul.traces = e.format_resultat(resultat)
    print(f"\nNote exercice : {resultat[0]}/{resultat[3]}")
    print(f"\nNote totale : {jul.note_totale}/{jul.total_points}")
    input("\nPressez Entrée pour continuer")

    # 3. Résultats
    print("\n+++++++++++++++++++\n")
    input(f"Voir les résulats de {rob.nom}, pressez une touche.")

    ExArithm1.clear()
    print(f"{rob.nom}, classe : {classe}")
    print(f"Total des points : {rob.note_totale}/{rob.total_points}")
    print(f"Détails des résulats :{rob.traces}")

    print("\n+++++++++++++++++++\n")
    input(f"Voir les résulats de {jul.nom}, pressez une touche.")

    ExArithm1.clear()
    print(f"{jul.nom}, classe : {classe}")
    print(f"Total des points : {jul.note_totale}/{jul.total_points}")
    print(f"Détails des résulats :{jul.traces}")
